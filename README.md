# Zapsi Tools
Software for testing devices from www.zapsi.eu

## Features
* realtime information from device
* delete data (from memory and sd card)
* set device time and date
* show device digital inputs
* show device analog inputs
* set device digital outputs
* show sd card data


www.zapsi.eu © 2017

Download : [Zapsi Tools.zip](https://gitlab.com/petrjahoda/ZapsiTools-JavaFX/uploads/75262ea49a6ed80acf0267af53c88092/Zapsi_Tools.exe)


![image 002](https://user-images.githubusercontent.com/11396401/32559964-b0f80582-c4a8-11e7-9357-772f55502f83.png)
