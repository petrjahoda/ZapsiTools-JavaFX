package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Controller implements Initializable {
    public Button connectButton;
    public TextField ipAddressField;
    public Text actualIPAddress;
    public Text actualMask;
    public Text actualGateway;
    public Text sdCardInfo;
    public Text actualTime;
    public Text actualTransformerValue;
    public Button setTimeButton;
    public Button deleteDataButton;
    public Text digitalDataInfo;
    public Text analogDataInfo;
    public RadioButton analogCheckBox;
    public RadioButton digitalCheckBox;
    public Text d1Value;
    public Text d2Value;
    public Text d3Value;
    public Text d4Value;
    public Text d5Value;
    public Text d6Value;
    public Text d7Value;
    public Text d8Value;
    public Text a1Value;
    public Text a2Value;
    public Text a3Value;
    public Text a4Value;
    public TextArea fileOutput;
    public Button out1Button;
    public Button out2Button;
    public Button out3Button;
    public Button out4Button;
    public Button out5Button;
    public Button out6Button;
    public Button out7Button;
    public Button out8Button;
    public Button setValueButton;
    static boolean zapsiSocketConnected = false;
    static Socket clientSocket;
    public Button jc10Button;
    public Button jc16Button;
    public Button jc24Button;
    public TextArea zapsiLog;
    public TextField valueField;
    public Text actualMAC;
    public Text actualZapsiInfo;
    public Button refreshButton;
    public Text out1Value;
    public Text out2Value;
    public Text out3Value;
    public Text out4Value;
    public Text out5Value;
    public Text out6Value;
    public Text out7Value;
    public Text out8Value;
    public Button updateButton;
    public Button selectFileButton;
    public TextField firmwareFileLocation;
    private String firmwareFile = "";
    private String log;
    private int actualNumberOfLinesInLog;
    private String ipAddress;
    private static BufferedReader inFromServer;
    static Timer statusDownloader;
    static Timer zapsiAliveChecker;
    static boolean timerIsRunning;
    private int analogRows = 0;
    private int digitalRows = 0;
    private String previousTime;
    private boolean firstConnect;
    private boolean zapsiIsAlive;
    private File file;

    public void initialize(URL location, ResourceBundle resources) {
        setGUI();
    }

    private void setGUI() {
        setButtonsDefaults();
        setTextDefaults();
    }

    private void downloadOutValues() {
        int port = 80;
        StringBuilder IOPage = new StringBuilder();
        try {
            int c;
            URL u = new URL("http://" + ipAddress + "/IO");
            if (u.getPort() != -1) {
                port = u.getPort();
            }
            if (!u.getProtocol().equalsIgnoreCase("http")) {
                System.err.println("Sorry. I only understand http.");
            }
            Socket s = new Socket(u.getHost(), port);
            OutputStream theOutput = s.getOutputStream();
            PrintWriter pw = new PrintWriter(theOutput, false);
            pw.print("GET " + u.getFile() + " HTTP/1.0\r\n");
            pw.print("Accept: text/plain, text/html, text/*\r\n");
            pw.print("\r\n");
            pw.flush();
            InputStream in = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            while ((c = br.read()) != -1) {
                IOPage.append((char) c);
            }
            parseIO(IOPage.toString());
            br.close();
            isr.close();
            in.close();
            pw.close();
            theOutput.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void parseIO(String IOPage) {
        IOPage = IOPage.substring(50);
        Pattern digitalOne = Pattern.compile("[1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
        Matcher matcher = digitalOne.matcher(IOPage);
        if (matcher.find()) {
            out1Value.setText("1");
        } else {
            out1Value.setText("0");
        }
        Pattern digitalTwo = Pattern.compile("[0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
        matcher = digitalTwo.matcher(IOPage);
        if (matcher.find()) {
            out2Value.setText("1");
        } else {
            out2Value.setText("0");
        }
        Pattern digitalThree = Pattern.compile("[0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
        matcher = digitalThree.matcher(IOPage);
        if (matcher.find()) {
            out3Value.setText("1");
        } else {
            out3Value.setText("0");
        }
        Pattern digitalFour = Pattern.compile("[0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9]");
        matcher = digitalFour.matcher(IOPage);
        if (matcher.find()) {
            out4Value.setText("1");
        } else {
            out4Value.setText("0");
        }
        Pattern digitalFive = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9]");
        matcher = digitalFive.matcher(IOPage);
        if (matcher.find()) {
            out5Value.setText("1");
        } else {
            out5Value.setText("0");
        }
        Pattern digitalSix = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9]");
        matcher = digitalSix.matcher(IOPage);
        if (matcher.find()) {
            out6Value.setText("1");
        } else {
            out6Value.setText("0");
        }
        Pattern digitalSeven = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9]");
        matcher = digitalSeven.matcher(IOPage);
        if (matcher.find()) {
            out7Value.setText("1");
        } else {
            out7Value.setText("0");
        }
        Pattern digitalEight = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1]");
        matcher = digitalEight.matcher(IOPage);
        if (matcher.find()) {
            out8Value.setText("1");
        } else {
            out8Value.setText("0");
        }
    }

    private void setTextDefaults() {
        actualIPAddress.setText("");
        actualMask.setText("");
        actualGateway.setText("");
        sdCardInfo.setText("");
        actualTime.setText("");
        actualTransformerValue.setText("");
        digitalDataInfo.setText("");
        analogDataInfo.setText("");
        actualMAC.setText("");
        actualZapsiInfo.setText("");
    }

    private void setButtonsDefaults() {
        ipAddressField.setStyle("-fx-background-radius: 3px 0px 0px 3px");
        connectButton.setStyle("-fx-background-radius: 0px 3px 3px 0px");
        firmwareFileLocation.setStyle("-fx-background-radius: 3px 0px 0px 3px");
        selectFileButton.setStyle("-fx-background-radius: 0px 3px 3px 0px");
        analogCheckBox.setSelected(true);
        connectButton.setDisable(true);
        setTimeButton.setDisable(true);
        deleteDataButton.setDisable(true);
        analogCheckBox.setDisable(true);
        digitalCheckBox.setDisable(true);
        out1Button.setDisable(true);
        out2Button.setDisable(true);
        out3Button.setDisable(true);
        out4Button.setDisable(true);
        out5Button.setDisable(true);
        out6Button.setDisable(true);
        out7Button.setDisable(true);
        out8Button.setDisable(true);
        jc10Button.setDisable(true);
        jc16Button.setDisable(true);
        jc24Button.setDisable(true);
        refreshButton.setDisable(true);
        updateButton.setDisable(true);
        selectFileButton.setDisable(true);
    }

    public void connectZapsi() throws IOException {
        if (InetAddress.getByName(ipAddress).isReachable(1000)) {
            if (connectButton.getText().equals("Connect")) {
                zapsiIsAlive = true;
                firstConnect = true;
                previousTime = "";
                analogRows = 0;
                digitalRows = 0;
                connectButton.setText("Disconnect");
                deleteDataButton.setDisable(false);
                setTimeButton.setDisable(false);
                analogCheckBox.setDisable(false);
                digitalCheckBox.setDisable(false);
                selectFileButton.setDisable(true);
                jc10Button.setDisable(false);
                jc16Button.setDisable(false);
                jc24Button.setDisable(false);
                out1Button.setDisable(false);
                out2Button.setDisable(false);
                out3Button.setDisable(false);
                out4Button.setDisable(false);
                out5Button.setDisable(false);
                out6Button.setDisable(false);
                out7Button.setDisable(false);
                out8Button.setDisable(false);
                setValueButton.setDisable(true);
                ipAddressField.setDisable(true);
                refreshButton.setDisable(false);
                updateButton.setDisable(true);
                downloadStatus();
                connectThroughPort10001();
                downloadOutValues();
                downloadAnalogAndDigitalData();
                checkZapsi();
            } else {
                disconnectZapsi();
            }
        } else {
            actualZapsiInfo.setText("Unreachable");
        }
    }


    private void parseInputs(String statusPage) {
        System.out.println(statusPage);
        if (firstConnect) {

            Pattern digitalInOne = Pattern.compile("[1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
            Matcher digitalInMatcher = digitalInOne.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d1Value.setText("1");
            } else {
                d1Value.setText("0");
            }
            Pattern digitalInTwo = Pattern.compile("[0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
            digitalInMatcher = digitalInTwo.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d2Value.setText("1");
            } else {
                d2Value.setText("0");
            }
            Pattern digitalInThree = Pattern.compile("[0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9]");
            digitalInMatcher = digitalInThree.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d3Value.setText("1");
            } else {
                d3Value.setText("0");
            }
            Pattern digitalInFour = Pattern.compile("[0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9][-][0-9]");
            digitalInMatcher = digitalInFour.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d4Value.setText("1");
            } else {
                d4Value.setText("0");
            }
            Pattern digitalInFive = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9][-][0-9]");
            digitalInMatcher = digitalInFive.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d5Value.setText("1");
            } else {
                d5Value.setText("0");
            }
            Pattern digitalInSix = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9][-][0-9]");
            digitalInMatcher = digitalInSix.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d6Value.setText("1");
            } else {
                d6Value.setText("0");
            }
            Pattern digitalInSeven = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1][-][0-9]");
            digitalInMatcher = digitalInSeven.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d7Value.setText("1");
            } else {
                d7Value.setText("0");
            }
            Pattern digitalInEight = Pattern.compile("[0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][0-9][-][1]");
            digitalInMatcher = digitalInEight.matcher(statusPage);
            if (digitalInMatcher.find()) {
                d8Value.setText("1");
            } else {
                d8Value.setText("0");
            }
            firstConnect = false;
        }

    }

    private void checkZapsi() {
        zapsiAliveChecker = new Timer();
        zapsiAliveChecker.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    if (!InetAddress.getByName(ipAddress).isReachable(2000)) {
                        zapsiIsAlive = false;
                        disconnectZapsi();
                        System.out.println(connectButton.getText());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println("ZAPSI IS ALIVE: " + zapsiIsAlive);
            }

        }, 0, 2500);
    }

    private void disconnectZapsi() throws IOException {
        refreshButton.setDisable(true);
        jc10Button.setDisable(true);
        jc16Button.setDisable(true);
        jc24Button.setDisable(true);
        analogCheckBox.setDisable(true);
        digitalCheckBox.setDisable(true);
        deleteDataButton.setDisable(true);
        selectFileButton.setDisable(false);
        setTimeButton.setDisable(true);
        actualTransformerValue.setText("");
        actualMAC.setText("");
        actualZapsiInfo.setText("");
        actualIPAddress.setText("");
        actualGateway.setText("");
        actualMask.setText("");
        sdCardInfo.setText("");
        actualTime.setText("");
        digitalDataInfo.setText("");
        analogDataInfo.setText("");
        ipAddressField.setDisable(false);
        out1Button.setDisable(true);
        out2Button.setDisable(true);
        out3Button.setDisable(true);
        out4Button.setDisable(true);
        out5Button.setDisable(true);
        out6Button.setDisable(true);
        out7Button.setDisable(true);
        out8Button.setDisable(true);
        d1Value.setText("0");
        d2Value.setText("0");
        d3Value.setText("0");
        d4Value.setText("0");
        d5Value.setText("0");
        d6Value.setText("0");
        d7Value.setText("0");
        d8Value.setText("0");
        out1Value.setText("0");
        out2Value.setText("0");
        out3Value.setText("0");
        out4Value.setText("0");
        out5Value.setText("0");
        out6Value.setText("0");
        out7Value.setText("0");
        out8Value.setText("0");
        a1Value.setText("0");
        a2Value.setText("0");
        a3Value.setText("0");
        a4Value.setText("0");
        if (firmwareFile.length() > 0) {
            updateButton.setDisable(false);
        }

        fileOutput.clear();
        zapsiLog.clear();
        if (Controller.timerIsRunning) {
            try {
                Controller.statusDownloader.cancel();
                Controller.zapsiAliveChecker.cancel();

            } catch (Exception e) {
                System.out.println("Cannot close controllers");
            }
        }
        if (Controller.zapsiSocketConnected) {
            try {
                Controller.clientSocket.close();
            } catch (IOException e) {
                System.out.println("\t\tCannot close socket");
            }
        }
        Controller.zapsiSocketConnected = false;

        Platform.runLater(() -> connectButton.setText("Connect"));
    }

    private void downloadAnalogAndDigitalData() {
        downloadDigitalDataStatus();
        downloadAnalogDataStatus();
    }

    private void downloadAnalogDataStatus() {
        int port = 80;
        StringBuilder analogData = new StringBuilder();
        try {
            int c;
            URL u = new URL("http://" + ipAddress + "/log/analog.txt");
            if (u.getPort() != -1) {
                port = u.getPort();
            }
            if (!u.getProtocol().equalsIgnoreCase("http")) {
                System.err.println("Sorry. I only understand http.");
            }
            Socket s = new Socket(u.getHost(), port);
            OutputStream theOutput = s.getOutputStream();
            PrintWriter pw = new PrintWriter(theOutput, false);
            pw.print("GET " + u.getFile() + " HTTP/1.0\r\n");
            pw.print("Accept: text/plain, text/html, text/*\r\n");
            pw.print("\r\n");
            pw.flush();
            InputStream in = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            int analogRowsInFile = 0;
            while ((c = br.read()) != -1) {
                if ((char) c == '\n') {
                    ++analogRowsInFile;
                }
                analogData.append((char) c);
            }
            if (analogData.toString().contains("404 Not Found")) {
                analogDataInfo.setText("no data");
            } else {
                analogRows = analogRowsInFile - 5;
                analogDataInfo.setText("" + analogRows + " records");
            }
            if (analogCheckBox.isSelected()) {
                fileOutput.setText(analogData.toString());
            }
            br.close();
            isr.close();
            in.close();
            pw.close();
            theOutput.close();
            s.close();
        } catch (MalformedURLException ex) {
            System.out.println("PROBLEM METHOD: analog data, malformedURLException");
        } catch (IOException ex) {
            System.out.println("PROBLEM METHOD: analog data, IOException");
        }
    }

    private void downloadDigitalDataStatus() {
        int port = 80;
        StringBuilder digitalData = new StringBuilder();
        try {
            int c;
            URL u = new URL("http://" + ipAddress + "/log/digital.txt");
            if (u.getPort() != -1) {
                port = u.getPort();
            }
            if (!u.getProtocol().equalsIgnoreCase("http")) {
                System.err.println("Sorry. I only understand http.");
            }
            Socket s = new Socket(u.getHost(), port);
            OutputStream theOutput = s.getOutputStream();
            PrintWriter pw = new PrintWriter(theOutput, false);
            pw.print("GET " + u.getFile() + " HTTP/1.0\r\n");
            pw.print("Accept: text/plain, text/html, text/*\r\n");
            pw.print("\r\n");
            pw.flush();
            InputStream in = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            int digitalRowsInFile = 0;
            while ((c = br.read()) != -1) {
                if ((char) c == '\n') {
                    ++digitalRowsInFile;
                }
                digitalData.append((char) c);
            }
            if (digitalData.toString().contains("404 Not Found")) {
                digitalRows = 0;
                digitalDataInfo.setText("no data");
            } else {
                digitalRows = digitalRowsInFile - 5;
                digitalDataInfo.setText("" + digitalRows + " records");
            }
            if (digitalCheckBox.isSelected()) {
                fileOutput.setText(digitalData.toString());
            }
            br.close();
            isr.close();
            in.close();
            pw.close();
            theOutput.close();
            s.close();
        } catch (MalformedURLException ex) {
            System.out.println("PROBLEM METHOD: digital data, malformedURLException");
        } catch (IOException ex) {
            System.out.println("PROBLEM METHOD: digital data, IOException");
        }
    }

    private void downloadStatus() {
        statusDownloader = new Timer();
        statusDownloader.schedule(new TimerTask() {

            @Override
            public void run() {
                if (zapsiIsAlive) {
                    Controller.timerIsRunning = true;
                    int port = 80;
                    StringBuilder statusPage = new StringBuilder();
                    try {
                        int c;
                        URL u = new URL("http://" + ipAddress + "/status");
                        if (u.getPort() != -1) {
                            port = u.getPort();
                        }
                        if (!u.getProtocol().equalsIgnoreCase("http")) {
                            System.err.println("Sorry. I only understand http.");
                        }
                        Socket s = new Socket(u.getHost(), port);
                        OutputStream theOutput = s.getOutputStream();
                        PrintWriter pw = new PrintWriter(theOutput, false);
                        pw.print("GET " + u.getFile() + " HTTP/1.0\r\n");
                        pw.print("Accept: text/plain, text/html, text/*\r\n");
                        pw.print("\r\n");
                        pw.flush();
                        InputStream in = s.getInputStream();
                        InputStreamReader isr = new InputStreamReader(in);
                        BufferedReader br = new BufferedReader(isr);
                        while ((c = br.read()) != -1) {
                            statusPage.append((char) c);
                        }
                        parseStatusInfoPage(statusPage.toString());
                        br.close();
                        isr.close();
                        in.close();
                        pw.close();
                        theOutput.close();
                        s.close();
                    } catch (MalformedURLException ex) {
                        System.out.println("PROBLEM METHOD: downloadStatus, malformedURLException");
                    } catch (IOException ex) {
                        System.out.println("PROBLEM METHOD: downloadStatus, IOException");
                    }
                    if (!zapsiSocketConnected) {
                        statusDownloader.cancel();
                        System.out.println("Cancelling status downloading");
                    }
                } else {
                    statusDownloader.cancel();
                }

            }
        }, 0, 1000);
    }

    private void parseStatusInfoPage(String statusPage) {
        actualIPAddress.setText(ipAddress);
        Pattern mac = Pattern.compile("[M][A][C].*");
        Matcher matcher = mac.matcher(statusPage);
        if (matcher.find()) {
            actualMAC.setText(matcher.group(0).substring(5));
        } else {
            actualMAC.setText("-");
        }
        Pattern mask = Pattern.compile("[S][M].*");
        matcher = mask.matcher(statusPage);
        if (matcher.find()) {
            actualMask.setText(matcher.group(0).substring(4));
        } else {
            actualMask.setText("-");
        }
        Pattern gateway = Pattern.compile("[G][W].*");
        matcher = gateway.matcher(statusPage);
        if (matcher.find()) {
            actualGateway.setText(matcher.group(0).substring(4));
        } else {
            actualGateway.setText("-");
        }
        Pattern sdCard = Pattern.compile(".*[K][B][\\s][a][v][a][i][l][a][b][l][e]");
        matcher = sdCard.matcher(statusPage);
        if (matcher.find()) {
            sdCardInfo.setText(matcher.group(0).substring(2));
            sdCardInfo.setStyle("-fx-fill: black");
        } else {
            sdCardInfo.setText("SC card not readable");
            sdCardInfo.setStyle("-fx-fill: red");
        }
        Pattern transformerValue = Pattern.compile("[t][i][o].*");
        matcher = transformerValue.matcher(statusPage);
        if (matcher.find()) {
            actualTransformerValue.setText(matcher.group(0).substring(7));
        } else {
            actualTransformerValue.setText("-");
        }
        String actualZapsiString = "";
        Pattern zapsiType = Pattern.compile("[Z][A].*\\d");
        matcher = zapsiType.matcher(statusPage);
        actualZapsiString = matcher.find() ? matcher.group(0) : actualZapsiString + "--";
        Pattern firmwareValue = Pattern.compile("[a][r][e].*");
        matcher = firmwareValue.matcher(statusPage);
        if (matcher.find()) {
            actualZapsiInfo.setText(actualZapsiString + ": fw" + matcher.group(0).substring(4));
        } else {
            actualZapsiInfo.setText("-");
        }
        Pattern zapsiTime = Pattern.compile("[R][T][C].*");
        matcher = zapsiTime.matcher(statusPage);
        if (matcher.find()) {
            actualTime.setText(matcher.group(0).substring(5));
            if (previousTime.equals(matcher.group(0).substring(5))) {
                actualTime.setStyle("-fx-fill: red");
                System.out.println("Z: " + matcher.group(0).substring(5));
                System.out.println("S: " + new SimpleDateFormat("d.M.yyyy   H:m:s").format(Calendar.getInstance().getTime()));
            } else if (!matcher.group(0).substring(5).equals(new SimpleDateFormat("d.M.yyyy   H:m:s").format(Calendar.getInstance().getTime()))) {
                actualTime.setStyle("-fx-fill: orange");
                System.out.println("Z: " + matcher.group(0).substring(5));
                System.out.println("S: " + new SimpleDateFormat("d.M.yyyy   H:m:s").format(Calendar.getInstance().getTime()));
            } else {
                actualTime.setStyle("-fx-fill: black");
                System.out.println("Z: " + matcher.group(0).substring(5));
                System.out.println("S: " + new SimpleDateFormat("d.M.yyyy   H:m:s").format(Calendar.getInstance().getTime()));
            }
            previousTime = matcher.group(0).substring(5);
        } else {
            actualTime.setText("-");
        }
        Pattern inputs = Pattern.compile("[I][n][p][u][t][s].*");
        matcher = inputs.matcher(statusPage);
        if (matcher.find()) {
            parseInputs(matcher.group(0).substring(14));
        }


    }

    private void connectThroughPort10001() {
        try {
            clientSocket = new Socket(ipAddressField.getText(), 10001);
            zapsiSocketConnected = true;
        } catch (Exception e) {
            System.out.println("PROBLEM METHOD: connectThroughPort10001, Exception");
            zapsiSocketConnected = false;
        }
        if (zapsiSocketConnected) {
            Runnable worker = () -> {
                System.out.println("Socket connected");
                log = "";
                actualNumberOfLinesInLog = 0;
                try {
                    inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    while (zapsiSocketConnected && zapsiIsAlive) {
                        processLine(inFromServer);
                    }
                } catch (IOException e) {
                    System.out.println("PROBLEM METHOD: connectThroughPort10001, IOException");

                }
            };
            new Thread(worker).start();
        }
    }

    private void processLine(BufferedReader inFromServer) {
        try {
            String actualLine = inFromServer.readLine();
            int MAX_NUMBER_OF_LINES = 15;
            if (actualNumberOfLinesInLog > MAX_NUMBER_OF_LINES) {
                log = deletefirstLineFromLog(log);
                --actualNumberOfLinesInLog;
            }
            Platform.runLater(() -> zapsiLog.setText(log)
            );
            log = log + actualLine + "\n";
            if (actualLine.contains("Econ")) {
                parseAnalog(actualLine);
            } else if (actualLine.contains("Zmena na vstupoch")) {
                parseDigital(actualLine);
            } else if (actualLine.contains("Alnalog")) {
                ++analogRows;
                analogDataInfo.setText("" + analogRows + " records");
            }
            ++actualNumberOfLinesInLog;
        } catch (Exception e) {
            System.out.println("Problem reading line: " + e);
        }
    }

    private String deletefirstLineFromLog(String log) {
        return log.substring(log.indexOf(10) + 1);
    }

    private void parseDigital(String actualLine) {
        Pattern digitalOne = Pattern.compile("[:][\\d][;]");
        Matcher matcher = digitalOne.matcher(actualLine);
        if (matcher.find()) {
            d1Value.setText(matcher.group(0).substring(1, 2));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d1Value.setText("0");
        Pattern digitalTwo = Pattern.compile("[:][x][;][\\d][;]");
        matcher = digitalTwo.matcher(actualLine);
        if (matcher.find()) {
            d2Value.setText(matcher.group(0).substring(3, 4));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d2Value.setText("0");
        Pattern digitalThree = Pattern.compile("[:][x][;][x][;][\\d][;]");
        matcher = digitalThree.matcher(actualLine);
        if (matcher.find()) {
            d3Value.setText(matcher.group(0).substring(5, 6));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d3Value.setText("0");
        Pattern digitalFour = Pattern.compile("[:][x][;][x][;][x][;][\\d][;]");
        matcher = digitalFour.matcher(actualLine);
        if (matcher.find()) {
            d4Value.setText(matcher.group(0).substring(7, 8));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d4Value.setText("0");
        Pattern digitalFive = Pattern.compile("[:][x][;][x][;][x][;][x][;][\\d][;]");
        matcher = digitalFive.matcher(actualLine);
        if (matcher.find()) {
            d5Value.setText(matcher.group(0).substring(9, 10));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d5Value.setText("0");
        Pattern digitalSix = Pattern.compile("[:][x][;][x][;][x][;][x][;][x][;][\\d][;]");
        matcher = digitalSix.matcher(actualLine);
        if (matcher.find()) {
            d6Value.setText(matcher.group(0).substring(11, 12));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d6Value.setText("0");
        Pattern digitalSeven = Pattern.compile("[:][x][;][x][;][x][;][x][;][x][;][x][;][\\d][;]");
        matcher = digitalSeven.matcher(actualLine);
        if (matcher.find()) {
            d7Value.setText(matcher.group(0).substring(13, 14));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
            return;
        }
        d7Value.setText("0");
        Pattern digitalEight = Pattern.compile("[:][x][;][x][;][x][;][x][;][x][;][x][;][x][;][\\d][;]");
        matcher = digitalEight.matcher(actualLine);
        if (matcher.find()) {
            d8Value.setText(matcher.group(0).substring(15, 16));
            ++digitalRows;
            digitalDataInfo.setText("" + digitalRows + " records");
        } else {
            d8Value.setText("0");
        }
    }

    private void parseAnalog(String actualLine) {
        Pattern analogOne = Pattern.compile("[I][1][=][\\s][\\d]*[.]*[\\d]*");
        Matcher matcher = analogOne.matcher(actualLine);
        if (matcher.find()) {
            a1Value.setText(matcher.group(0).substring(4));
            return;
        }
        a1Value.setText("-");
        Pattern analogTwo = Pattern.compile("[I][2][=][\\s][\\d]*[.]*[\\d]*");
        matcher = analogTwo.matcher(actualLine);
        if (matcher.find()) {
            a2Value.setText(matcher.group(0).substring(4));
            return;
        }
        a2Value.setText("-");
        Pattern analogThree = Pattern.compile("[I][3][=][\\s][\\d]*[.]*[\\d]*");
        matcher = analogThree.matcher(actualLine);
        if (matcher.find()) {
            a3Value.setText(matcher.group(0).substring(4));
            return;
        }
        a3Value.setText("-");
        Pattern analogFour = Pattern.compile("[I][4][=][\\s][\\d]*[.]*[\\d]*");
        matcher = analogFour.matcher(actualLine);
        if (matcher.find()) {
            a4Value.setText(matcher.group(0).substring(4));
        } else {
            a4Value.setText("-");
        }
    }

    public void setTime() {
        setTimeButton.setDisable(true);
        if (zapsiSocketConnected) {
            try {
                DatagramSocket serverSocket = new DatagramSocket(9999);
                InetAddress IPAddress = Inet4Address.getByName(ipAddressField.getText());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
                SimpleDateFormat dayOfWeek = new SimpleDateFormat("u");
                Date date = new Date();
                String sendString = "set_datetime=" + dateFormat.format(date) + " 0" + dayOfWeek.format(date) + "&";
                System.out.println(sendString);
                byte[] sendData = sendString.getBytes(StandardCharsets.UTF_8);
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9999);
                serverSocket.send(sendPacket);
                serverSocket.close();
            } catch (SocketException e) {
                System.out.println("PROBLEM METHOD: setTime, socketException");
            } catch (UnknownHostException e) {
                System.out.println("PROBLEM METHOD: setTime, unknownHosException");
            } catch (UnsupportedEncodingException e) {
                System.out.println("PROBLEM METHOD: setTime, unsupportedEncodingException");
            } catch (IOException e) {
                System.out.println("PROBLEM METHOD: setTime, IOException");
            }
        }
        setTimeButton.setDisable(false);
    }

    public void deleteData() {
        try {
            URL analogData = new URL("http://" + ipAddress + "/log/analog.txt");
            HttpURLConnection connectionAnalog = (HttpURLConnection) analogData.openConnection();
            connectionAnalog.setRequestMethod("DELETE");
            int responseCodeFromAnalog = connectionAnalog.getResponseCode();
            System.out.println("ANALOG response code: " + responseCodeFromAnalog);
            connectionAnalog.disconnect();
            URL digitalData = new URL("http://" + ipAddress + "/log/digital.txt");
            HttpURLConnection connectionDigital = (HttpURLConnection) digitalData.openConnection();
            connectionDigital.setRequestMethod("DELETE");
            int responseCodeFromDigital = connectionDigital.getResponseCode();
            System.out.println("DIGITAL response code: " + responseCodeFromDigital);
            connectionDigital.disconnect();
            analogRows = 0;
            digitalRows = 0;
            analogDataInfo.setText("no data");
            digitalDataInfo.setText("no data");
            downloadDigitalDataStatus();
            downloadAnalogDataStatus();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Data deleted");
    }

    public void checkAnalog() {
        analogCheckBox.setSelected(true);
        digitalCheckBox.setSelected(false);
    }

    public void checkDigital() {
        analogCheckBox.setSelected(false);
        digitalCheckBox.setSelected(true);
    }

    public void ipAddressEntered() {
        if (!zapsiSocketConnected) {
            if (ipAddressField.getText().matches("^[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}$")) {
                connectButton.setDisable(false);
                selectFileButton.setDisable(false);
                ipAddress = ipAddressField.getText();
                selectFileButton.setDisable(false);
                if (firmwareFile.length() > 0) {
                    updateButton.setDisable(false);
                }
            } else {
                connectButton.setDisable(true);
                selectFileButton.setDisable(true);
                selectFileButton.setDisable(true);
                updateButton.setDisable(true);
            }
        }
    }

    public void ipAddressFieldEntered() throws IOException {
        if (!connectButton.isDisabled()) {
            connectZapsi();
        }
    }

    public void setJC10() {
        jc10Button.setDisable(true);
        int ratio = 10;
        updateTransformerValue(ratio);
        jc10Button.setDisable(false);
    }

    public void setJC16() {
        jc16Button.setDisable(true);
        int ratio = 16;
        updateTransformerValue(ratio);
        jc16Button.setDisable(false);
    }

    public void setJC24() {
        jc24Button.setDisable(true);
        int ratio = 24;
        updateTransformerValue(ratio);
        jc24Button.setDisable(false);
    }

    private void updateTransformerValue(int ratio) {
        try {
            String sendString = "SetCTratio" + Character.toString((char) ratio) + Character.toString((char) ratio) + Character.toString((char) ratio);
            PrintWriter output = new PrintWriter(clientSocket.getOutputStream());
            output.println(sendString);
            output.flush();
            output.close();
        } catch (Exception e) {
            System.out.println("PROBLEM METHOD: updateTransformerValue");
        }
    }

    public void setValue() {
        if (valueField.getText().matches("^(?:(?!0)\\d{1,2}|100)$")) {
            int ratio = Integer.valueOf(valueField.getText());
            updateTransformerValue(ratio);
        }
    }

    public void checkRatio() {
        if (valueField.getText().matches("^(?:(?!0)\\d{1,2}|100)$")) {
            setValueButton.setDisable(false);
        } else {
            setValueButton.setDisable(true);
        }
    }

    private void changeRelay(String relayValue, Text relay) {
        String message;
        boolean dataToSend;
        if (relay.getText().equals("1")) {
            message = "CLR /Rele" + relayValue;
            dataToSend = false;
        } else {
            message = "SET /Rele" + relayValue;
            dataToSend = true;
        }
        if (dataToSend) {
            while (relay.getText().equals("0")) {
                sendMessage(message);
            }
        } else {
            while (relay.getText().equals("1")) {
                sendMessage(message);
            }
        }
        downloadOutValues();
    }

    private void sendMessage(String message) {
        Socket socket = new Socket();
        try {
            socket.connect(new InetSocketAddress(ipAddress, 80));
            PrintWriter s_out = new PrintWriter(socket.getOutputStream(), true);
            s_out.println(message);
            s_out.close();
            socket.close();
            downloadOutValues();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOut1() {
        changeRelay("1", out1Value);
    }

    public void setOut2() {
        changeRelay("2", out2Value);
    }

    public void setOut3() {
        changeRelay("3", out3Value);
    }

    public void setOut4() {
        changeRelay("4", out4Value);
    }

    public void setOut5() {
        changeRelay("5", out5Value);
    }

    public void setOut6() {
        changeRelay("6", out6Value);
    }

    public void setOut7() {
        changeRelay("7", out7Value);
    }

    public void setOut8() {
        changeRelay("8", out8Value);
    }

    public void refreshData() {
        downloadAnalogAndDigitalData();
    }

    static {
        timerIsRunning = false;
    }


    private boolean checkForZapsi() {
        System.out.println("Initial Zapsi check.");
        boolean itIsZapsi = false;
        int port = 80;
        StringBuilder statusPage = new StringBuilder();
        try {
            int c;
            URL u = new URL("http://" + ipAddress + "/status");
            if (u.getPort() != -1) {
                port = u.getPort();
            }
            if (!u.getProtocol().equalsIgnoreCase("http")) {
                System.err.println("Sorry. I only understand http.");
            }
            Socket s = new Socket(u.getHost(), port);
            OutputStream theOutput = s.getOutputStream();
            PrintWriter pw = new PrintWriter(theOutput, false);
            pw.print("GET " + u.getFile() + " HTTP/1.0\r\n");
            pw.print("Accept: text/plain, text/html, text/*\r\n");
            pw.print("\r\n");
            pw.flush();
            InputStream in = s.getInputStream();
            InputStreamReader isr = new InputStreamReader(in);
            BufferedReader br = new BufferedReader(isr);
            while ((c = br.read()) != -1) {
                statusPage.append((char) c);
            }
            String page = String.valueOf(statusPage);
            if (page.contains("ZAPSI 5")) {
                itIsZapsi = true;
            }
            br.close();
            isr.close();
            in.close();
            pw.close();
            theOutput.close();
            s.close();
        } catch (MalformedURLException ex) {
            System.out.println("PROBLEM METHOD: downloadStatus, malformedURLException");
        } catch (IOException ex) {
            System.out.println("PROBLEM METHOD: downloadStatus, IOException");
        }
        if (itIsZapsi) {
            System.out.println("Zapsi is online, starting process.");
        } else {
            System.out.println("Zapsi is not online.");
        }
        return itIsZapsi;
    }

    public void selectFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Zapsi Firmware", "*.bin"));
        file = fileChooser.showOpenDialog(new Stage());
        if (file != null) {
            firmwareFile = file.getAbsolutePath();
            firmwareFileLocation.setText(file.getAbsolutePath());
            updateButton.setDisable(false);
        }
    }

    public void updateFirmware() throws IOException, InterruptedException {
        connectButton.setDisable(true);
        selectFileButton.setDisable(true);
        boolean zapsiIsConnected = checkForZapsi();
        if (zapsiIsConnected) {
            downloadFirmwareBackup();
            sendNewFirmware();
            downloadNewFirmware();
//            compareFiles();
            startBootloader();
            while (InetAddress.getByName(ipAddress).isReachable(1000)) {
                System.out.print(".");
            }
            Thread.sleep(5000);
            boolean ready = testForReadyCommand();

            if (ready) {
                deleteUpdateFlag();
            }
        }
        connectButton.setDisable(false);
        selectFileButton.setDisable(false);

    }

    private boolean testForReadyCommand() {
        System.out.println("Testing for ready command.");
        boolean ready = false;
        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet("http://" + ipAddress + "/Test"))) {
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            System.out.println(responseXml);
            System.out.println("Zapsi is ready");
            ready = true;
        } catch (IOException e) {
            System.out.println("Zapsi not ready.");
            e.printStackTrace();
        }
        return ready;
    }

    private void deleteUpdateFlag() {
        System.out.println("Finalizing.");

        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet("http://" + ipAddress + "/FlasingOK"))) {
            String responseXml = EntityUtils.toString(response.getEntity());
            EntityUtils.consume(response.getEntity());
            System.out.println(responseXml);
            System.out.println("Zapsi firmware successfully update");
        } catch (IOException e) {
            System.out.println("Problem while flashing Zapsi, please reboot.");
            e.printStackTrace();
        }
    }

    private void startBootloader() {
        System.out.println("Flashing Zapsi with new firmware.");

        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet("http://" + ipAddress + "/RunBootloader"))) {
            HttpEntity entity = response.getEntity();
            System.out.println(entity.getContent());
            System.out.println("Flash was successfull");
        } catch (IOException e) {
            System.out.println("Problem while flashing Zapsi, please reboot.");
            e.printStackTrace();
        }
    }

    private void downloadNewFirmware() {
        System.out.println("Downloading new firmware from Zapsi for comparison.");
        File myFile = new File("uploaded.bin");

        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet("http://" + ipAddress + "/Partition1"))) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (FileOutputStream outstream = new FileOutputStream(myFile)) {
                    entity.writeTo(outstream);
                }
                System.out.println("New firmware successfully downloaded.");
            }
        } catch (IOException e) {
            System.out.println("Problem with new firmware download.");
            e.printStackTrace();
        }
    }

    private void sendNewFirmware() {
        System.out.println("Sending new firmware to Zapsi.");

//             Version 1
//        try (CloseableHttpClient ignored = HttpClients.createDefault()) {
//            HttpEntity data = MultipartEntityBuilder.create()
//                    .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
//                    .addBinaryBody("dummy.txt", file.getAbsoluteFile())
//                    .build();
//
//            HttpUriRequest request = RequestBuilder
//                    .post("http://" + ipAddress + "/Partition1")
//                    .setEntity(data)
//                    .build();
//
//            System.out.println("Executing request " + request.getRequestLine());
//            System.out.println("New firmware uploaded to Zapsi");
//        } catch (IOException e) {
//            System.out.println("Problem with firmware upload");
//            e.printStackTrace();
//        }
        String url = "http://" + ipAddress + "/Partition1";
        CloseableHttpClient httpclient = HttpClientBuilder.create().build();
        HttpPost httppost = new HttpPost(url);

        System.out.println(url);
        System.out.println(file.getAbsolutePath());
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.addPart("dummy.txt", new FileBody(file.getAbsoluteFile()));

        HttpEntity entity = builder.build();

        httppost.setEntity(entity);

        try {
            HttpResponse response = httpclient.execute(httppost);
            System.out.println(response);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private void downloadFirmwareBackup() {
        System.out.println("Downloading backup firmware from Zapsi.");
        File myFile = new File("backup.bin");

        CloseableHttpClient client = HttpClients.createDefault();
        try (CloseableHttpResponse response = client.execute(new HttpGet("http://" + ipAddress + "/MCU"))) {
            HttpEntity entity = response.getEntity();
            if (entity != null) {
                try (FileOutputStream outstream = new FileOutputStream(myFile)) {
                    entity.writeTo(outstream);
                }
                System.out.println("Backup firmware successfully downloaded.");
            }
        } catch (IOException e) {
            System.out.println("Problem with firmware download.");
            e.printStackTrace();
        }


    }
}

