package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Main extends Application {

    public static final String BUILD_NUMBER = "72";
    public static final String BUILD_DATE = "20180728-075709";

    @Override
    public void start(Stage primaryStage) throws Exception {

        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd-HHmmss");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd. MM. yyyy,  HH:mm:ss");
        Date date = format.parse(BUILD_DATE);
        String outDateVersion = outputFormat.format(date);
        Parent root = null;
        try {
            root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        } catch (IOException e) {
            System.out.println("Problem loading FXML");
            e.printStackTrace();
        }
        primaryStage.setTitle("Zapsi Tools 1.4." + BUILD_NUMBER + "      (" + outDateVersion + ")");
        assert (root != null);
        primaryStage.setScene(new javafx.scene.Scene(root, 800.0D, 700.0D));
        primaryStage.setResizable(false);
        primaryStage.getIcons().add(new Image(sample.Main.class.getResourceAsStream("icon.png")));
        primaryStage.show();
    }

    public void stop() {
        System.out.println("Close while running");
        if (Controller.timerIsRunning) {
            Controller.statusDownloader.cancel();
            Controller.zapsiAliveChecker.cancel();
        }
        if (Controller.zapsiSocketConnected) {
            try {
                Controller.clientSocket.close();
            } catch (IOException e) {
                System.out.println("\t\tCannot close socket");
            }
        }
        try {
        Controller.zapsiSocketConnected = false;

        } catch (Exception e) {
            System.out.println("Cannot set zapsisocket co false");
        }
    }


    public static void main(String[] args) {
        launch(args);
    }
}
